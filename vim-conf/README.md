# VIM - configuration files

These are my favorite CLI and GUI vim configurations for C, CUDA, Java,
Python, Ruby, Bash, TCL, Latex, Latex bib, Makefile, Markdown and text source
files. Tabs are expanded to spaces and a tab is 4 spaces. Programming languages
have a ruler, denoting the limit for characters per line, most of them having a
limit of 80 characters while higher-level programming languages having 100.
