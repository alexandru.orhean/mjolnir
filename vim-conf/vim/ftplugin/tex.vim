set showmatch
set tabstop=4
set shiftwidth=4
set softtabstop=0
set smarttab
set expandtab
set textwidth=120
"set spell
set spelllang=en
