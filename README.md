# README

Author: Alexandru Iulian Orhean, PhD student at Illinois Institute of Technology

Email: aorhean@hawk.iit.edu / alexandru.orhean@gmail.com /
alexandru.orhean@outlook.com

Mjolnir (The Hammer of Thor, one of the most fearsome and powerful weapons in 
existence, capable of leveling mountains) is a collection of scripts,
programs and configuration files accumulated throughout the years, while
setting up different systems and applications.Here is a list of systems and 
applications, including descriptions, that can be found in this repository:

## Ceph - distributed storage cluster

The *ceph-conf* directory contains an experimental script designed to deploy the
dependecies and Ceph on a 3 node cluster. The cluster has only one LAN and each
node has a single network interface. Access to the Internet is achieved through
a router. The nodes are homogeneous (4 Cores, 8GB RAM, 80GB SSD, and 200GB
network storage), are called *greencrowbar-[#]* and operate under Fedora 26
(Linux kernel 4.13.9). The experiment consists of deploying Ceph from an admin
node on all of the other nodes (including the admin node) through the use of
ceph-deploy, setting up a Ceph Filesystem, and then testing the fault tolerance
and performance in the scenario of one node crashing/failing.

## LDAP SSH PUB KEY Authentication

TODO - add description

## Luster - parallel file system cluster

TODO - add description

## VIM - configuration for favorite editor

The *vim-conf* directory contains CLI and GUI vim configurations.

## Git - class scripts (deprecated)

The *git-scripts* directory contains general scripts and programs used across 
the years to automate some of the management and homework assignment processes 
in various classes.

## School Helper Scripts

The *school-helper-scripts* directory contains a bunch of scripts used to automate the cloning, update, collection 
and confirmation of receipt of homework assigments of students from class, when using git repositories.
