#!/bin/bash

# add the base domain name
ldapadd -x -D cn=admin,dc=example,dc=com -W -f base.ldif

# search the configuration database of LDAP server
ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config dn

# add the ssh key object class and attributes to LDAP schema
ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f scripts/98ssh.ldif

# add new user to LDAP
ldapadd -x -D cn=admin,dc=example,dc=com -W -f user.ldif
