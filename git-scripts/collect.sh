#!/bin/bash

REPO_DIR_PATH=
REPO_STORE_PATH=
REPO_REMOTE_PATH=
MON=$(date | tr -s ' ' | cut -d ' ' -f3)
DAY=$(date | tr -s ' ' | cut -d ' ' -f2)
OWN_USER=
OWN_GROUP=
SSH_CMD=

TEMP=$(echo "$DAY" | grep "^[0-9]*$")
if [ "$TEMP" == "" ]
then
    TEMP=$DAY
    DAY=$MON
    MON=$TEMP
fi

log=logs/$MON-$DAY.log

cd $REPO_DIR_PATH

# update the local "actual" repositories
echo "*** updating the repos ..." > $log
declare -A rcs
while read name
do
    cdir=$(pwd)
    cd repos-actual/$name
    #git fetch
    rc=$(git pull | tail -n -1 2>> ../../$log)
    echo "$name $rc" >> ../../$log
    if [ "$rc" == "Already up-to-date." ]
    then
        rcs[$name]+="0"
    else
        rcs[$name]+="1"
    fi
    cd $cdir
done < groups-list.txt
echo "... done updating the repos!" >> $log

# copy the repositories to local storage
echo "*** copying the changed repos to local storage ..." >> $log
mkdir -p $REPO_STORE_PATH/$MON-$DAY
while read dname
do
    echo "## $dname ${rcs[$dname]}" >> $log
    if [ ${rcs[$dname]} -ne 0 ]
    then
        rsync -rvzh repos-actual/$dname $REPO_STORE_PATH/$MON-$DAY/ &>> $log
    fi
done < groups-list.txt
chown -R $OWN_USER:$OWN_GROUP $REPO_STORE_PATH/$MON-$DAY
chmod g+rwx $REPO_STORE_PATH/$MON-$DAY
echo "... done copying the changed repos to local storage!" >> $log

# copy the repositories to remote backup
echo "*** copying the repos to remote backup ..." >> $log
rsync -rvzhe "$SSH_CMD" $REPO_STORE_PATH/$MON-$DAY $REPO_REMOTE_PATH &>> $log
echo "... done copying the repos to remote backup!" >> $log

