#!/bin/bash

DEBUG=0
HELP=0
FILE=""
DEST=""
LINK=""

# parse arguments
params=($@)
i=0
while [ $i -lt $# ]
do
    if [ ${params[$i]} == "-x" ]
    then
        DEBUG=1
        i=$[$i + 1]
        continue
    fi

    if [ ${params[$i]} == "-i" ]
    then
        FILE=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-d" ]
    then
        DEST=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi
    
    if [ ${params[$i]} == "-l" ]
    then
        LINK=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-h" ]
    then
        HELP=1
        break
    fi
done

# test arguments
if [ $HELP -eq 1 ]
then
    echo "Usage: ./clone-repos.sh [ARGUMENT [PARAMETER]]"
    echo ""
    echo "ARGUMENT: -x              Debug mode"
    echo "          -i [FILE]       Input file"
    echo "          -d [DEST]       Destination Path"
    echo "          -l [LINK]       Source Link"
    echo "          -h              Help"
    echo ""
    exit 0
fi

if [ "$FILE" == "" ]
then
    echo "Input file argument is required!"
    exit -1
fi

if [ "$DEST" == "" ]
then
    echo "Destination path argument is required!"
    exit -1
fi

if [ "$LINK" == "" ]
then
    echo "Source link argument is required!"
    exit -1
fi

# read input file and clone repositories in specified destination path
while read name
do
    cmd="git clone $LINK/$name.git $DEST/$name"
    echo $cmd
    if [ $DEBUG -eq 0 ]
    then
        eval $cmd
    fi
done < $FILE

