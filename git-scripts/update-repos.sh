#!/bin/bash

DEBUG=0
HELP=0
FILE=""
DEST=""

function print_help()
{
    echo "Usage: ./update-repos.sh [ARGUMENT [PARAMETER]]"
    echo ""
    echo "ARGUMENT: -x              Debug mode"
    echo "          -i [FILE]       Input file"
    echo "          -d [DEST]       Destination Path"
    echo "          -h              Help"
    echo ""
}

# parse arguments
params=($@)
i=0
while [ $i -lt $# ]
do
    if [ ${params[$i]} == "-x" ]
    then
        DEBUG=1
        i=$[$i + 1]
        continue
    fi

    if [ ${params[$i]} == "-i" ]
    then
        FILE=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-d" ]
    then
        DEST=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-h" ]
    then
        HELP=1
        break
    fi
done

# test arguments
if [ $HELP -eq 1 ]
then
    print_help
    exit 0
fi

if [ "$FILE" == "" ]
then
    echo "Input file is required!"
    print_help
    exit -1
fi

if [ "$DEST" == "" ]
then
    echo "Destination path is required!"
    print_help
    exit -1
fi

# read input file and update the repositories in specified destination path
while read name
do
    cdir=$(pwd)
    cmd1="cd $DEST/$name"
    cmd2="git fetch"
    cmd3="git pull"
    cmd4="cd $cdir"
    for i in {1..4}
    do
        cmd=cmd${i}
        echo ${!cmd}
        if [ $DEBUG -eq 0 ]
            then
            eval ${!cmd}
        fi
    done
done < $FILE

