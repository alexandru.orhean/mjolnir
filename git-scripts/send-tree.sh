#!/bin/bash

DEBUG=0
HELP=0
DEST=""
FILE=""
SENDER=""
HEADER=""
FOOTER=""
HW=""

# parse arguments
params=($@)
i=0
while [ $i -lt $# ]
do
    if [ ${params[$i]} == "-x" ]
    then
        DEBUG=1
        i=$[$i + 1]
        continue
    fi

    if [ ${params[$i]} == "-i" ]
    then
        FILE=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-s" ]
    then
        SENDER=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi
    
    if [ ${params[$i]} == "-e" ]
    then
        HEADER=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-m" ]
    then
        FOOTER=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-w" ]
    then
        HW=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi
    
    if [ ${params[$i]} == "-d" ]
    then
        DEST=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi
    
    if [ ${params[$i]} == "-u" ]
    then
        SUBJECT=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-h" ]
    then
        HELP=1
        break
    fi
done

# test arguments
if [ $HELP -eq 1 ]
then
    echo "Usage: ./send-tree.sh [ARGUMENT [PARAMETER]]"
    echo ""
    echo "ARGUMENT: -x              Debug mode"
    echo "          -i [FILE]       Input file"
    echo "          -s [SENDER]     Sender email address"
    echo "          -e [HEADER]     Email header"
    echo "          -m [FOOTER]     Email footer"
    echo "          -u [SUBJECT]    Email subject"
    echo "          -w [HOMEWORK]   Homework directory"
    echo "          -d [DEST]       Destination path"
    echo "          -h              Help"
    echo ""
    exit 0
fi

if [ "$FILE" == "" ]
then
    echo "Input file argument is required!"
    exit -1
fi

if [ "$SENDER" == "" ]
then
    echo "Sender argument is required!"
    exit -1
fi

if [ "$HW" == "" ]
then
    echo "Homework argument is required!"
    exit -1
fi

if [ "$DEST" == "" ]
then
    echo "Destination path argument is required!"
    exit -1
fi

# read input file and clone repositories in specified destination path
while read group name
do
    echo -n "" > temp.txt
    cat $HEADER >> temp.txt
    tree -sh $DEST/$group/$HW | tail -n +2 >> temp.txt
    echo "" >> temp.txt
    cat $FOOTER >> temp.txt
    echo "" >> temp.txt
    cmd="cat temp.txt | mail -s \"$SUBJECT $HW confirmation\" -r $SENDER $name"
    echo $cmd
    if [ $DEBUG -eq 0 ]
    then
        eval $cmd
    else
        cat temp.txt
    fi
done < $FILE

