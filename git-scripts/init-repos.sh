#!/bin/bash

DEBUG=0
HELP=0
FILE_LIST=""
FILE_DIRS=""
DEST=""

MSG="TODO: write me"
TXT="readme.txt"

function print_help()
{
    echo "Usage: ./init-repos.sh [ARGUMENT [PARAMETER]]"
    echo ""
    echo "ARGUMENT: -x              Debug mode"
    echo "          -i [FILE]       Input file with the list of names"
    echo "          -j [FILE]       Input file with the list of directories"
    echo "          -d [DEST]       Destination Path"
    echo "          -h              Help"
    echo ""
}

# parse arguments
params=($@)
i=0
while [ $i -lt $# ]
do
    if [ ${params[$i]} == "-x" ]
    then
        DEBUG=1
        i=$[$i + 1]
        continue
    fi

    if [ ${params[$i]} == "-i" ]
    then
        FILE_LIST=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi
    
    if [ ${params[$i]} == "-j" ]
    then
        FILE_DIRS=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-d" ]
    then
        DEST=${params[$[$i + 1]]}
        i=$[$i + 2]
        continue
    fi

    if [ ${params[$i]} == "-h" ]
    then
        HELP=1
        break
    fi
done

# test arguments
if [ $HELP -eq 1 ]
then
    print_help
    exit 0
fi

if [ "$FILE_LIST" == "" ]
then
    echo "Input file with list of names is required!"
    print_help
    exit -1
fi

if [ "$FILE_DIRS" == "" ]
then
    echo "Input file with list of directories is required!"
    print_help
    exit -1
fi

if [ "$DEST" == "" ]
then
    echo "Destination path is required!"
    print_help
    exit -1
fi

# read input file and initialize the repositories in specified destination path
while read name
do
    commited_items=""
    no_added_items=0
    cdir=$(pwd)
    while read dname
    do
        if [ ! -e "$DEST/$name/$dname/$TXT" ]
        then
            cmd1="mkdir -p $DEST/$name/$dname"
            cmd2="touch $DEST/$name/$dname/$TXT"
            cmd3="echo \"$MSG\" > $DEST/$name/$dname/$TXT"
            cmd4="cd $DEST/$name"
            cmd5="git add $dname/$TXT"
            commited_items="$commited_items $dname"
            cmd6="cd $cdir"
            no_added_items=$(($no_added_items + 1))
            for i in {1..6}
            do
                cmd=cmd${i}
                echo ${!cmd}
                if [ $DEBUG -eq 0 ]
                then
                    eval ${!cmd}
                fi
            done
        fi
    done < $FILE_DIRS
    if [ $no_added_items -gt 0 ]
    then
        cmd1="cd $DEST/$name"
        cmd2="git commit -m \"added readme for:$commited_items\""
        cmd3="git push"
        cmd4="cd $cdir"
        for i in {1..4}
        do
            cmd=cmd${i}
            echo ${!cmd}
            if [ $DEBUG -eq 0 ]
            then
                eval ${!cmd}
            fi
        done
    fi
done < $FILE_LIST

