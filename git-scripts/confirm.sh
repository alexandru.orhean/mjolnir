#!/bin/bash

REPO_DIR_PATH=
SENDER=
HEADER=header.txt
MATRIX_QUOTE=matrix-quote.txt

MON=$(date | tr -s ' ' | cut -d ' ' -f3)
DAY=$(date | tr -s ' ' | cut -d ' ' -f2)

TEMP=$(echo "$DAY" | grep "^[0-9]*$")
if [ "$TEMP" == "" ]
then
    TEMP=$DAY
    DAY=$MON
    MON=$TEMP
fi

log=logs/$MON-$DAY-confirmation.log

# send email confirmation with hw submission contents
cd $REPO_DIR_PATH
echo "*** sending emails ..." > $log
./scripts/send-tree.sh \
        -i students-list.txt \
        -s $SENDER \
        -e "$HEADER" \
        -m "$MATRIX_QUOTE" \
        -d repos-actual \
        -u "[Class]" \
        -w project &>> $log
echo "... done sending email!" >> $log
