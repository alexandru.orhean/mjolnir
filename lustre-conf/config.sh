#!/bin/bash

# network configuration
yum -y install vim bash-completion net-tools epel-release

sed -i 's/NM_CONTROLLED="yes"/NM_CONTROLLED="no"/g' \
/etc/sysconfig/network-scripts/ifcfg-eth0

head -n 7 /etc/sysconfig/network-scripts/ifcfg-eth0 > \
/etc/sysconfig/network-scripts/ifcfg-eth1

sed -i 's/DEVICE="eth0"/DEVICE="eth1"/g' \
/etc/sysconfig/network-scripts/ifcfg-eth1

echo "DNS1=8.8.8.8" >> /etc/sysconfig/network-scripts/ifcfg-eth0

ifdown eth0

ifup eth0
ifup eth1

# disable selinux (requires reboot)
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' \
/etc/sysconfig/selinux

sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' \
/etc/selinux/config

reboot

# open port 988 for lustre
iptables -A INPUT -i eth1 -m state --state NEW,ESTABLISHED,RELATED -p tcp --dport 988 -j ACCEPT

# install dependencies
yum -y install wget perl libgssglue net-snmp libyaml sg3_utils openmpi lsof \
rsync

yum -y update kernel-firmware

# set up non-loop entry in /etc/hosts
ip=$(ip addr show dev eth1 | head -n 3 | tail -n 1 | cut -d ' ' -f6 \
| cut -d '/' -f1)

hostname=$(hostname)

echo "$ip    $hostname" >> /etc/hosts

# add lustre entry
echo "options lnet networks=tcp(eth1)" > /etc/modprobe.d/lustre.conf

# download and set up lustre server
wget --no-check-certificate https://downloads.hpdd.intel.com/public/lustre/lustre-2.7.0/el6.6/server/RPMS/x86_64/kernel-2.6.32-504.8.1.el6_lustre.x86_64.rpm

rpm -ivh kernel-2.6.32-504.8.1.el6_lustre.x86_64.rpm                             

/sbin/new-kernel-pkg --package kernel --mkinitrd --dracut --depmod \             
--install 2.6.32-504.8.1.el6_lustre.x86_64

wget --no-check-certificate https://downloads.hpdd.intel.com/public/lustre/lustre-2.7.0/el6.6/server/RPMS/x86_64/lustre-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm
wget --no-check-certificate https://downloads.hpdd.intel.com/public/lustre/lustre-2.7.0/el6.6/server/RPMS/x86_64/lustre-iokit-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm
wget --no-check-certificate https://downloads.hpdd.intel.com/public/lustre/lustre-2.7.0/el6.6/server/RPMS/x86_64/lustre-modules-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm
wget --no-check-certificate https://downloads.hpdd.intel.com/public/lustre/lustre-2.7.0/el6.6/server/RPMS/x86_64/lustre-osd-ldiskfs-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm
wget --no-check-certificate https://downloads.hpdd.intel.com/public/lustre/lustre-2.7.0/el6.6/server/RPMS/x86_64/lustre-osd-ldiskfs-mount-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm
wget --no-check-certificate https://downloads.hpdd.intel.com/public/lustre/lustre-2.7.0/el6.6/server/RPMS/x86_64/lustre-tests-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm
                                                                                 
wget --no-check-certificate https://downloads.hpdd.intel.com/public/e2fsprogs/latest/el6/RPMS/x86_64/e2fsprogs-1.42.13.wc5-7.el6.x86_64.rpm
wget --no-check-certificate https://downloads.hpdd.intel.com/public/e2fsprogs/latest/el6/RPMS/x86_64/e2fsprogs-libs-1.42.13.wc5-7.el6.x86_64.rpm
wget --no-check-certificate https://downloads.hpdd.intel.com/public/e2fsprogs/latest/el6/RPMS/x86_64/libcom_err-1.42.13.wc5-7.el6.x86_64.rpm
wget --no-check-certificate https://downloads.hpdd.intel.com/public/e2fsprogs/latest/el6/RPMS/x86_64/libss-1.42.13.wc5-7.el6.x86_64.rpm
                                                                                 
rpm -Uvh e2fsprogs-1.42.13.wc5-7.el6.x86_64.rpm \
e2fsprogs-libs-1.42.13.wc5-7.el6.x86_64.rpm \
libcom_err-1.42.13.wc5-7.el6.x86_64.rpm libss-1.42.13.wc5-7.el6.x86_64.rpm
                                                                                 
rpm -ivh lustre-modules-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm        
rpm -ivh lustre-osd-ldiskfs-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm       
rpm -ivh lustre-osd-ldiskfs-mount-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm
rpm -ivh lustre-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm                
rpm -ivh lustre-iokit-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm          
rpm -ivh lustre-tests-2.7.0-2.6.32_504.8.1.el6_lustre.x86_64.x86_64.rpm

# download and set up lustre client
wget --no-check-certificate http://vault.centos.org/6.6/updates/x86_64/Packages/kernel-2.6.32-504.8.1.el6.x86_64.rpm

rpm -ivh kernel-2.6.32-504.8.1.el6.x86_64.rpm

wget --no-check-certificate https://downloads.hpdd.intel.com/public/lustre/lustre-2.7.0/el6.6/client/RPMS/x86_64/lustre-client-modules-2.7.0-2.6.32_504.8.1.el6.x86_64.x86_64.rpm
wget --no-check-certificate https://downloads.hpdd.intel.com/public/lustre/lustre-2.7.0/el6.6/client/RPMS/x86_64/lustre-client-2.7.0-2.6.32_504.8.1.el6.x86_64.x86_64.rpm

rpm -ivh lustre-client-modules-2.7.0-2.6.32_504.8.1.el6.x86_64.x86_64.rpm
rpm -ivh lustre-client-2.7.0-2.6.32_504.8.1.el6.x86_64.x86_64.rpm
