#!/bin/bash

HNAME=""
ANAME=""

uname=""
email=""
passwd="$(pwgen -1sBv 8)"
homedir=""
sshkey=""
maaskey=""

read -p "Username: " uname
read -p "Email address: " email
read -p "SSH Public Key: " sshkey
homedir="/home/$uname"

useradd -m -s /bin/bash $uname
if [ $? -ne 0 ]
then
    echo "Error adding the new user!"
    exit
fi

mkdir $homedir/.ssh

echo $sshkey > $homedir/.ssh/authorized_keys
chmod 600 $homedir/.ssh/authorized_keys
chown $uname:$uname $homedir/.ssh/authorized_keys

cmd="ssh-keygen -q -t rsa -N \"\" -C \"$uname@$HNAME\" -f $homedir/.ssh/$HNAME"
eval $cmd
maaskey=$(cat $homedir/.ssh/$HNAME.pub)
chmod 600 $homedir/.ssh/$HNAME
chmod 644 $homedir/.ssh/$HNAME.pub
chown $uname:$uname $homedir/.ssh/$HNAME
chown $uname:$uname $homedir/.ssh/$HNAME.pub

echo "Host 192.168.128.*" > $homedir/.ssh/config
echo "    IdentityFile ~/.ssh/$HNAME" >> $homedir/.ssh/config
echo "Host *.maas" >> $homedir/.ssh/config
echo "    IdentityFile ~/.ssh/$HNAME" >> $homedir/.ssh/config
chown $uname:$uname $homedir/.ssh/config

chmod 700 $homedir/.ssh
chown $uname:$uname $homedir/.ssh

api_key_file="temp.txt"
maas-region apikey --username=$ANAME > $api_key_file
maas login $ANAME http://localhost:5240/MAAS/api/2.0 - < $api_key_file
maas $ANAME users create username=$uname email=$email \
    password=$passwd is_superuser=0
maas logout $ANAME

maas-region apikey --username=$uname > $api_key_file
maas login $uname http://localhost:5240/MAAS/api/2.0 - < $api_key_file
cmd="maas $uname sshkeys create \"key=$sshkey\""
eval $cmd
cmd="maas $uname sshkeys create \"key=$maaskey\""
eval $cmd
maas logout $uname

rm $api_key_file

echo "User password: $passwd"
