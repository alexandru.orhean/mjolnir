#!/bin/bash

USAGE="./confirm-repos.sh <project-student list file> <repos dir> <sender> <header file> <epilogue file> <subject> <homework> <log file>"

if [ $# -ne 8 ]
then
    echo "$USAGE"
    exit 1
fi

PROJECT_STUDENT_LIST_FILE=$1
REPOS_DIR=$2
SENDER=$3
HEADER_FILE=$4
EPILOGUE_FILE=$5
SUBJECT=$6
HOMEWORK=$7
LOG_FILE=$8
TEMP_FILE=/tmp/email_body.tmp

echo -n "" > $LOG_FILE

while read link project email
do
    echo -n "" > $TEMP_FILE

    if [ -d $REPOS_DIR/$project ]
    then
        cmd="cat $HEADER_FILE >> $TEMP_FILE"
        echo "$(date) [INFO] $cmd" >> $LOG_FILE
        eval $cmd &>> $LOG_FILE

        cmd="tree -sh $REPOS_DIR/$project | tail -n +2 >> $TEMP_FILE"
        echo "$(date) [INFO] $cmd" >> $LOG_FILE
        eval $cmd &>> $LOG_FILE

        cmd="cat $EPILOGUE_FILE >> $TEMP_FILE"
        echo "$(date) [INFO] $cmd" >> $LOG_FILE
        eval $cmd &>> $LOG_FILE

        cmd="cat $TEMP_FILE | mail -s \"$SUBJECT $HOMEWORK confirmation\" -r $SENDER $email"
        echo "$(date) [INFO] $cmd" >> $LOG_FILE
        eval $cmd &>> $LOG_FILE

        cat $TEMP_FILE >> $LOG_FILE
    fi
done < $PROJECT_STUDENT_LIST_FILE
