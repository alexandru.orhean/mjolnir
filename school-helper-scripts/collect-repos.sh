#!/bin/bash

USAGE="./collect-repos.sh <git project list file> <repos dir> <collect dir> <log file>"

if [ $# -ne 4 ]
then
    echo "$USAGE"
    exit 1
fi

PROJECT_LIST_FILE=$1
REPOS_DIR=$2
COLLECT_DIR=$3
LOG_FILE=$4

echo -n "" > $LOG_FILE

cmd="cd $REPOS_DIR"
echo "$(date) [INFO] $cmd" >> $LOG_FILE
eval $cmd &>> $LOG_FILE

while read link project
do
    if [ -d $REPOS_DIR/$project ]
    then
        cmd="cp -r $project $COLLECT_DIR/$project"
        echo "$(date) [INFO] $cmd" >> $LOG_FILE
        eval $cmd &>> $LOG_FILE

        cmd="chmod -R ug+rw $COLLECT_DIR/$project"
        echo "$(date) [INFO] $cmd" >> $LOG_FILE
        eval $cmd &>> $LOG_FILE

        echo "" >> $LOG_FILE
    fi
done < $PROJECT_LIST_FILE
