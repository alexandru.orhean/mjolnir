#!/bin/bash

USAGE="./update-repos.sh <git project list file> <repos dir> <log file>"

if [ $# -ne 3 ]
then
    echo "$USAGE"
    exit 1
fi

PROJECT_LIST_FILE=$1
REPOS_DIR=$2
LOG_FILE=$3

echo -n "" > $LOG_FILE
while read link project
do
    if [ -d $REPOS_DIR/$project ]
    then
        cmd="cd $REPOS_DIR/$project"
        echo "$(date) [INFO] $cmd" >> $LOG_FILE
        eval $cmd &>> $LOG_FILE

        cmd="git fetch & git pull"
        echo "$(date) [INFO] $cmd" >> $LOG_FILE
        eval $cmd &>> $LOG_FILE
        
        echo "" >> $LOG_FILE
    fi
done < $PROJECT_LIST_FILE
